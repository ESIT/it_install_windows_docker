# Install Docker On Winodws

Lab : Installing Docker On windows

---

## Instructions

 - Navigate to Docker Site 
```
https://docs.docker.com/docker-for-windows/install/#what-to-know-before-you-install
```

 - Download and Install the docker package 
```
https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe
```

 - Run the installation 
```
Click Next Until you will be asked if you want to install Windows container - Check the box and click next
https://kumarvikram.com/wp-content/uploads/2018/08/2018-08-06-1.png 
```

 - Wait for Docker to start
```
You can see the Docker Icone near the Clock 
```

 - Make sure that Docker is running 
```
Open Powershell and type docker version and see if you get the result 
**Note: if you dont get result you may need to restart the computer 

```
